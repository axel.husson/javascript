// Hey there!
// This is CODE, lets you control your character with code.
// If you don't know how to code, don't worry, It's easy.
// Just set attack_mode to true and ENGAGE!

var attack_mode=false
var max_gold = 60000
var state = "farm"

function farm() {
	start_character("RangerAx", 6)
	start_character("CromAx", 6)
	start_character("ResAx", 2)
}

function transferhPotions() {
	send_item("CromAx", "hpot0", Math.floor(quantity("hpot0") / 3));
	game_log("Potion sent to CromAx");
	send_item("ResAx", "hpot0", Math.floor(quantity("hpot0") / 3));
	game_log("Potion sent to ResAx");
	send_item("RangerAx", "hpot0", Math.floor(quantity("hpot0") / 3));
	game_log("Potion sent to RangerAx");
}

function transfermPotions() {
	send_item("CromAx", "mpot0", Math.floor(quantity("mpot0") / 3));
	game_log("Potion sent to CromAx");
	send_item("ResAx", "mpot0", Math.floor(quantity("mpot0") / 3));
	game_log("Potion sent to ResAx");
	send_item("RangerAx", "mpot0", Math.floor(quantity("mpot0") / 3));
	game_log("Potion sent to RangerAx");
}

function bank_deposit(gold)
{
    if(!character.bank) return game_log("Not inside the bank");
    parent.socket.emit("bank",{operation:"deposit",amount:gold});
}

function bank_withdraw(gold)
{
    if(!character.bank) return game_log("Not inside the bank");
    parent.socket.emit("bank",{operation:"withdraw",amount:gold});
}

function routine()
{
	var x=character.real_x,y=character.real_y,map=character.map;
	smart_move({to:"potions"},function(done){
		game_log("Arrivé");
		buy("hpot0",600);
		buy("mpot0",600);
		smart_move({to:"bank"},function(done){
			game_log("Arrivé");
			depot=character.gold - 9000;
			game_log("Dépot a la banque de " + depot + " gold","#4CE0CC");
        	bank_deposit(depot);
			smart_move({x:x,y:y,map:map}); //Return back to the original position
			});
		});
}

setInterval(function () {
//Determine what state we should be in.
	state_controller();
	
	//Switch statement decides what we should do based on the value of 'state'
	switch(state)
	{
		case "farm":
			break;
		case "routine":
			break;
	}
}, 100);//Execute 10 times per second

function state_controller()
{
	//Default to farming
	var new_state = "farm";
		
		if(character.gold > 70000)
		{
			new_state = "routine";
		}
	
	if(state != new_state)
	{
		state = new_state;
	}
	
}

setInterval(function(){

	use_hp_or_mp();
	loot();
	
	if(!attack_mode || character.rip || is_moving(character)) return;
	
	

	var target=get_targeted_monster();
	if(!target)
	{
		target=get_nearest_monster({min_xp:100,max_att:120});
		if(target) change_target(target);
		else
		{
			set_message("No Monsters");
			return;
		}
	}
	
	if(!is_in_range(target))
	{
		move(
			character.x+(target.x-character.x)/2,
			character.y+(target.y-character.y)/2
			);
		// Walk half the distance
	}
	else if(can_attack(target))
	{
		set_message("Attacking");
		attack(target);
	}
},1000/4); // Loops every 1/4 seconds.

// Learn Javascript: https://www.codecademy.com/learn/learn-javascript
// Write your own CODE: https://github.com/kaansoral/adventureland
