/** @param {NS} ns */
export async function main(ns) {
	while(ns.hacknet.getPurchaseNodeCost() != 'Infinity'){
		await ns.sleep(1000)
		var argent = ns.getServerMoneyAvailable('home');
		for (var a = 0; a < ns.hacknet.numNodes(); ++a){
			var argent = ns.getServerMoneyAvailable('home');
			if(argent>ns.hacknet.getLevelUpgradeCost(a,1)){
				ns.hacknet.upgradeLevel(a,1);
				ns.print("Achat level du node : " + a);
			}
			if(argent>ns.hacknet.getRamUpgradeCost(a,1)){
				ns.hacknet.upgradeRam(a,1);
				ns.print("Achat RAM du node : " + a);
			}
			if(argent>ns.hacknet.getCoreUpgradeCost(a,1)){
				ns.hacknet.upgradeCore(a,1);
				ns.print("Achat Core du node : " + a);
			}
			/*if(argent>ns.hacknet.getCacheUpgradeCost(a,1)){
				ns.hacknet.upgradeCache(a,1);
				ns.print("Achat Cache du node : " + a);
			}*/
		}
		if(argent > ns.hacknet.getPurchaseNodeCost()){
			ns.hacknet.purchaseNode();
		}
	}

	ns.alert('Fin achat infrastructure')
}	