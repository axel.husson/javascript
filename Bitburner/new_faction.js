/** @param {NS} ns */
export async function main(ns) {
	while(true){
		await ns.sleep(1000)
			//Accept Compagny factions
	const invitations = await ns.singularity.checkFactionInvitations();
		for (const invit of invitations){
			ns.alert("Recrutement dans la faction : " + invit);
			ns.singularity.joinFaction(invit);
		}
	}

}