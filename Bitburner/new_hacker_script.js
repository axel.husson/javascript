function scan(ns, parent, server, list) {
    const children = ns.scan(server);
    for (let child of children) {
        if (parent == child) {
            continue;
        }
        list.push(child);
        
        scan(ns, server, child, list);
    }
}

export function list_servers(ns) {
    const list = [];
    scan(ns, '', 'home', list);
    return list;
}

/** @param {NS} ns */
export async function main(ns) {
	const servers = list_servers(ns);
	var tor = false
	while (true){

		for(const server of servers) {
			//destroy bitnode
			if (ns.getHackingLevel() > ns.getServerRequiredHackingLevel('w0r1d_d43m0n')){
				ns.exec('destroy.js', 'home')
			}
			

            //mise en place des variables
		    var player = ns.getPlayer();
		    
			var Max_Ram = await ns.getServerMaxRam('home')
			var thread = parseInt(Max_Ram / 10);
			while (ns.hacknet.numHashes() > 4){
			ns.hacknet.spendHashes("Sell for Money");
			}

			var argent = ns.getServerMoneyAvailable('home');
			//achat pour home
			if (argent > ns.singularity.getUpgradeHomeRamCost()){
				ns.singularity.upgradeHomeRam()
			}
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > ns.singularity.getUpgradeHomeCoresCost()){
				ns.singularity.upgradeHomeCores()
			}
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > 200000 && !tor){
				ns.singularity.purchaseTor()
				var tor = true
			}
			ns.print("Argent : " + argent)
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > 5000000 && tor && !ns.fileExists("relaySMTP.exe")){
				ns.singularity.purchaseProgram("relaySMTP.exe");
			}
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > 30000000 && tor && !ns.fileExists("HTTPWorm.exe")){
				ns.singularity.purchaseProgram("HTTPWorm.exe");
			}
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > 250000000 && tor && !ns.fileExists("SQLInject.exe")){
				ns.singularity.purchaseProgram("SQLInject.exe");
			}
			
			//Hacking Auto des serveurs
			var droitRoot = ns.hasRootAccess(server);
			var argent = ns.getServerMaxMoney(server);
			if(droitRoot){
				//await ns.scp("find_coding_contract.js", server);
				//await ns.exec("find_coding_contract.js", server);
				if(argent != 0){
					var Limit_Ram = (1.75 * parseInt(thread))
					var rep2 = true
					while(rep2){
						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						ns.print('Free Ram : ' + Free_Ram)
						ns.print('Limit_Ram : ' + Limit_Ram)
						var moneyThresh = ns.getServerMaxMoney(server) * 0.75;
						var securityThresh = ns.getServerMinSecurityLevel(server) + 5;
						ns.print('Droit root OK sur :' + server)
						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						if (Free_Ram > Limit_Ram){
							ns.exec("new_hack.script", 'home', thread, server);
						}
						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						if (ns.getServerSecurityLevel(server) > securityThresh && Free_Ram > Limit_Ram) {
							ns.exec("weaken.script", 'home', thread, server);
						}
						
						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						if (ns.getServerMoneyAvailable(server) < moneyThresh && Free_Ram > Limit_Ram){
							ns.exec("grow.script", 'home', thread, server);
						}

						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						if (ns.getServerSecurityLevel(server) > securityThresh && Free_Ram > Limit_Ram) {
							ns.exec("weaken.script", 'home', thread, server);
						}
						var rep2 = false
						await ns.sleep(2000);
					}
				}
			}else{
				var niveauPlayer = ns.getHackingLevel();
				var niveauServer = ns.getServerRequiredHackingLevel(server);
				
				if(niveauPlayer>=niveauServer){
					//ns.tprint('Serveur Hackable : ' + server);
					var nbrPort = ns.getServerNumPortsRequired(server);
					//ns.tprint('Nombre de port à hacker : ' + nbrPort);
					var nbrexe = 0;
					if(ns.fileExists("BruteSSH.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("FTPCrack.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("relaySMTP.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("HTTPWorm.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("SQLInject.exe")){
						nbrexe += 1;
					}
					//ns.tprint('Nombre de fichier executable: ' + nbrexe)
					if(nbrPort <= nbrexe){
						//ns.tprint('Ajout des droits root');
						while(!droitRoot){
							//ns.tprint('Brute SSH');
							if(ns.fileExists("BruteSSH.exe")){
								ns.brutessh(server);
								//ns.tprint('Brute FTP');
							}
							if(ns.fileExists("FTPCrack.exe")){
								ns.ftpcrack(server);
								//ns.tprint('Brute FTP');
							}
							if(ns.fileExists("relaySMTP.exe")){
								ns.relaysmtp(server);
								//ns.tprint('Brute SMTP');
							}
							if(ns.fileExists("HTTPWorm.exe")){
								ns.httpworm(server);
								//ns.tprint('Brute SMTP');
							}
							if(ns.fileExists("SQLInject.exe")){
								ns.sqlinject(server);
							}
							ns.nuke(server);
							//ns.tprint('Nuke');
							var droitRoot = ns.hasRootAccess(server);
						}
						if(argent != 0){
							await ns.scp("early-hack-template.script", server);
							//ns.tprint('depot fichier ok');
							var maxRam = ns.getServerMaxRam(server);
							var thread2 = parseInt(maxRam / 3);
							//ns.tprint('Nombre de thread utilisé : ' + thread)
							//ns.alert('Installation de la backdoor a distance sur ' + server);
							await ns.exec("install_backdoor_dist.js", 'home', 1, server);
							await ns.singularity.connect('home');
							if(thread2 > 0){
								ns.exec("early-hack-template.script", server, thread2);
							}
						}else{
							var ram = ns.getServerMaxRam(server);
							ns.alert('Serveur sans argent trouvé : ' + server + ' - Ram dispo : ' + ram);
							await ns.scp("server-hack.js", server)
							await ns.scp("grow.script", server)
							await ns.scp("weaken.script", server)
							await ns.scp("new_hack.script", server)
							await ns.scp("install_bacdoor.js", server)
							var Use_Ram = await ns.getServerUsedRam('home')
							var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
							while (Free_Ram < 6){
								await ns.sleep(2000);
								var Use_Ram = await ns.getServerUsedRam('home')
								var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
							}
							//ns.alert('Installation de la backdoor a distance sur ' + server);
							await ns.exec("install_backdoor_dist.js", 'home', 1, server);
							await ns.singularity.connect('home');					
						}
					}else{
						//ns.tprint('Impossible de hacker');
					}
				}
	
			}
		}

		await ns.sleep(1000)
	}

}