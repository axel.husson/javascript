var playerServers = ['home'];
var serverChecked = [];
var checkList = [];
/** @param {NS} ns **/
export async function main(ns) {
    await ServersScan(ns, 'home');
    await printArray(ns, serverChecked);
}
/** @param {NS} ns **/
async function ServersScan(ns, target) {
    var servers1 = await ns.scan(target);
    for (var server in servers1) {
        //await ns.tprint(server)
        if (!checkList.includes(servers1[server])) {
            checkList.push(servers1[server]);
        }
    }
    serverChecked.push(target);
    var flag = true;
    while (flag) {
        flag = false;
        for (var i = 0; i < checkList.length; i++) {
            var servers = await ns.scan(checkList[i]);
            if (!serverChecked.includes(checkList)) {
                serverChecked.push(checkList[i]);
            }
            for (var server in servers) {
                if (!checkList.includes(servers[server])) {
                    checkList.push(servers[server]);
                }
            }
        }
    }
    // remove player servers from serverChecked
    for (var server in playerServers) {
        for (var i = 0; i < serverChecked.length; i++) {
            if (serverChecked == playerServers[server]) {
                serverChecked.splice(i, 1);
                i--;
            }
        }
    }
}

async function printArray(ns, serverList) {
	while(true){
        var hostname = ns.getHostname();
        var Max_Ram = await ns.getServerMaxRam(hostname)
		var thread = parseInt(Max_Ram / 40); 
		for (var server in serverList) {
			//var niveauPlayer = ns.getHackingLevel();
			//ns.tprint(serverList[server]);
			var droitRoot = ns.hasRootAccess(serverList[server]);
			var argent = ns.getServerMaxMoney(serverList[server]);
			if(droitRoot){
				if(argent != 0){
					var moneyThresh = ns.getServerMaxMoney(serverList[server]) * 0.75;
					var securityThresh = ns.getServerMinSecurityLevel(serverList[server]) + 5;
					ns.print('Droit root OK sur :' + serverList[server])
					if (ns.getServerSecurityLevel(serverList[server]) > securityThresh) {
						ns.exec("weaken.script", hostname, thread, serverList[server]);
						await ns.sleep(2000);
					}
					if (ns.getServerMoneyAvailable(serverList[server]) < moneyThresh){
						ns.exec("grow.script", hostname, thread, serverList[server]);
						await ns.sleep(2000);
					}
					ns.exec("new_hack.script", hostname, thread, serverList[server]);
					await ns.sleep(2000);
					if (ns.getServerSecurityLevel(serverList[server]) > securityThresh) {
						ns.exec("weaken.script", hostname, thread, serverList[server]);
						await ns.sleep(2000);
					}
				}
			}
		}
	await ns.sleep(2000);
    }
}