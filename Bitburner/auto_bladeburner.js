function scan(ns, parent, server, list) {
    const children = ns.scan(server);
    for (let child of children) {
        if (parent == child) {
            continue;
        }
        list.push(child);
        
        scan(ns, server, child, list);
    }
}

export function list_servers(ns) {
    const list = [];
    scan(ns, '', 'home', list);
    return list;
}

/** @param {NS} ns */
export async function main(ns) {

	//ns.exec("infrastructure.js", 'home')
	//ns.exec("custom-stat.js", 'home')

	const servers = list_servers(ns);
	const Factions = [];
	var Choix = false;
	var limit_rep = 14000
	//Accept Compagny factions
	const invitations = await ns.checkFactionInvitations
	for (const invit of invitations){
		ns.alert("Recrutement dans la faction : " + invit);
		ns.joinFaction(invit);
		Factions.push(invit);
	}
	await ns.sleep(1000)
	//Liste des villes
	const Villes1 = ['Sector-12', 'Chongqing', 'Volhaven', 'Aevum']
	//liste des entreprise par villes
	const Villes = {'Sector-12' : ['MegaCorp', 'Four Sigma', 'Blade Industries'],
					'Chongqing' : ['KuaiGong International'],
					'Volhaven' : ['NWO', 'OmniTek Incorporated'],
					'Aevum' : ['ECorp', 'Bachman & Associates', 'Clarke Incorporated', 'Fulcrum Technologies']}
	//je parcours la liste des villes
	for (const Ville of Villes1){
		//je parcours les entreprise de chaques villes
		const Industries = Villes[Ville]
		for (const industrie of Industries){
			//Check si je fais partie des factions des entreprise
			if(industrie == 'Fulcrum Technologies'){
				var faction = 'Fulcrum Secret Technologies'
			}else{
				var faction = industrie
			}
			var reput = ns.getFactionRep(faction)
			//Si je ne fais pas partie de la faction
			if (reput == 0){
				//Je stock les infos le nom de l'entreprise et la ville ou elle se trouve
				var entreprise = industrie;
				var cityName = Ville;
				//je sors de la boucle... Peut être...
				ns.alert('Choix de l entreprise : ' + entreprise)
				break;
			}
		}
	}
	//Je voyage vers la bonne ville
	ns.travelToCity(cityName)

	
    while (true){
		//await ns.exec("find_coding_contract.js", 'home');
		//Choix de l'augmentation
		//list de nos augmentations
		const Augments = await ns.getOwnedAugmentations()
		var nombreaugment = Augments.length
		var nombre_hacknode = ns.hacknet.numNodes()

		//Si on à des factions et qu'on n'a pas fais notre choix
		if (Factions.length != 0 && !Choix){
			for (const faction of Factions){
				const facaugments = ns.getAugmentationsFromFaction(faction)
				const facnotpurchasedaugments = facaugments.filter(item => !Augments.includes(item))
				//check si il y a des augmentations a acheter dans la faction
				if (facnotpurchasedaugments.length != 0){
					//Si y en a 
					for (const facnotpurchasedaugment of facnotpurchasedaugments){
						const Pre_Req = ns.getAugmentationPrereq(facnotpurchasedaugment)
						//et qu'il n'y a pas de pré requis
						if (Pre_Req.length == 0){
							var factionwork = faction;
							var prereq_augment = ns.getAugmentationCost(facnotpurchasedaugments[0])
							var limit_rep_augment = prereq_augment[0]
							var prix_augment = prereq_augment[1]
							var augment = facnotpurchasedaugments[0]
							//je fais mon choix
							var Choix = true;
							ns.alert('Vous allez travailler pour la faction ' + factionwork + ' Pour acheter l augmentation ' + augment)
						}
					}
				}				
			}
		}
		

        for(const server of servers) {
            //mise en place des variables
		    var player = ns.getPlayer();
		    var argent = ns.getServerMoneyAvailable('home');
			var Max_Ram = await ns.getServerMaxRam('home')
			var thread = parseInt(Max_Ram / 10);
			//achat pour home
			if (argent > ns.getUpgradeHomeRamCost()){
				ns.upgradeHomeRam()
			}
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > ns.getUpgradeHomeCoresCost()){
				ns.upgradeHomeCores()
			}
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > 200000 && !player.tor){
				ns.purchaseTor()
			}
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > 5000000 && player.tor && !ns.fileExists("relaySMTP.exe")){
				ns.purchaseProgram("relaySMTP.exe");
			}
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > 30000000 && player.tor && !ns.fileExists("HTTPWorm.exe")){
				ns.purchaseProgram("HTTPWorm.exe");
			}
			var argent = ns.getServerMoneyAvailable('home');
			if (argent > 250000000 && player.tor && !ns.fileExists("SQLInject.exe")){
				ns.purchaseProgram("SQLInject.exe");
			}
			//actions joueur
			/*if (ns.isBusy()){
				if (player.hp = player.maxHp || player.hp < 9){
					ns.bladeburner.stopBladeburnerAction()
				}
			}else{
				if (player.hp = player.maxHp){
					ns.bladeburner.startAction("general", "Training");
				}else{
					ns.bladeburner.startAction("Contracts", "Tracking");
				}
			}*/
			//Check invitation Faction
			var invitation = await ns.checkFactionInvitations()
			if (invitation[0]){
				var faction = invitation[0].toString();
				ns.alert("Recrutement dans la faction : " + faction);
				ns.joinFaction(faction);
				Factions.push(faction);
			}
			//Hacking Auto des serveurs
			var droitRoot = ns.hasRootAccess(server);
			var argent = ns.getServerMaxMoney(server);
			if(droitRoot){
				//await ns.scp("find_coding_contract.js", server);
				//await ns.exec("find_coding_contract.js", server);
				if(argent != 0){
					var Limit_Ram = (1.75 * parseInt(thread))
					var rep2 = true
					while(rep2){
						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						ns.print('Free Ram : ' + Free_Ram)
						ns.print('Limit_Ram : ' + Limit_Ram)
						var moneyThresh = ns.getServerMaxMoney(server) * 0.75;
						var securityThresh = ns.getServerMinSecurityLevel(server) + 5;
						ns.print('Droit root OK sur :' + server)
						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						if (Free_Ram > Limit_Ram){
							ns.exec("new_hack.script", 'home', thread, server);
						}
						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						if (ns.getServerSecurityLevel(server) > securityThresh && Free_Ram > Limit_Ram) {
							ns.exec("weaken.script", 'home', thread, server);
						}
						
						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						if (ns.getServerMoneyAvailable(server) < moneyThresh && Free_Ram > Limit_Ram){
							ns.exec("grow.script", 'home', thread, server);
						}

						var Use_Ram = await ns.getServerUsedRam('home')
						var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
						if (ns.getServerSecurityLevel(server) > securityThresh && Free_Ram > Limit_Ram) {
							ns.exec("weaken.script", 'home', thread, server);
						}
						var rep2 = false
						await ns.sleep(2000);
					}
				}
			}else{
				var niveauPlayer = ns.getHackingLevel();
				var niveauServer = ns.getServerRequiredHackingLevel(server);
				
				if(niveauPlayer>=niveauServer){
					//ns.tprint('Serveur Hackable : ' + server);
					var nbrPort = ns.getServerNumPortsRequired(server);
					//ns.tprint('Nombre de port à hacker : ' + nbrPort);
					var nbrexe = 0;
					if(ns.fileExists("BruteSSH.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("FTPCrack.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("relaySMTP.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("HTTPWorm.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("SQLInject.exe")){
						nbrexe += 1;
					}
					//ns.tprint('Nombre de fichier executable: ' + nbrexe)
					if(nbrPort <= nbrexe){
						//ns.tprint('Ajout des droits root');
						while(!droitRoot){
							//ns.tprint('Brute SSH');
							if(ns.fileExists("BruteSSH.exe")){
								ns.brutessh(server);
								//ns.tprint('Brute FTP');
							}
							if(ns.fileExists("FTPCrack.exe")){
								ns.ftpcrack(server);
								//ns.tprint('Brute FTP');
							}
							if(ns.fileExists("relaySMTP.exe")){
								ns.relaysmtp(server);
								//ns.tprint('Brute SMTP');
							}
							if(ns.fileExists("HTTPWorm.exe")){
								ns.httpworm(server);
								//ns.tprint('Brute SMTP');
							}
							if(ns.fileExists("SQLInject.exe")){
								ns.sqlinject(server);
							}
							ns.nuke(server);
							//ns.tprint('Nuke');
							var droitRoot = ns.hasRootAccess(server);
						}
						if(argent != 0){
							await ns.scp("early-hack-template.script", server);
							//ns.tprint('depot fichier ok');
							var maxRam = ns.getServerMaxRam(server);
							var thread2 = maxRam / 3;
							//ns.tprint('Nombre de thread utilisé : ' + thread)
							//ns.alert('Installation de la backdoor a distance sur ' + server);
							await ns.exec("install_backdoor_dist.js", 'home', 1, server);
							await ns.connect('home');
							if(thread2 > 0){
								ns.exec("early-hack-template.script", server, thread2);
							}
						}else{
							var ram = ns.getServerMaxRam(server);
							ns.alert('Serveur sans argent trouvé : ' + server + ' - Ram dispo : ' + ram);
							await ns.scp("server-hack.js", server)
							await ns.scp("grow.script", server)
							await ns.scp("weaken.script", server)
							await ns.scp("new_hack.script", server)
							await ns.scp("install_bacdoor.js", server)
							var Use_Ram = await ns.getServerUsedRam('home')
							var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
							while (Free_Ram < 6){
								await ns.sleep(2000);
								var Use_Ram = await ns.getServerUsedRam('home')
								var Free_Ram = parseInt(Max_Ram) - parseInt(Use_Ram)
							}
							//ns.alert('Installation de la backdoor a distance sur ' + server);
							await ns.exec("install_backdoor_dist.js", 'home', 1, server);
							await ns.connect('home');					
						}
					}else{
						//ns.tprint('Impossible de hacker');
					}
				}
	
			}
		}
	await ns.sleep(2000);
    }	
}