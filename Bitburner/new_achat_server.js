/** @param {NS} ns */
export async function main(ns) {
	var ram = 2048;
	var servers = ns.getPurchasedServers();
	var serv = servers.length;
	
	while(serv < ns.getPurchasedServerLimit()){
		await ns.sleep(1000)
		var argent = ns.getServerMoneyAvailable('home');
		ns.print('Prix serveur : ' + ns.getPurchasedServerCost(ram))
		if (argent > ns.getPurchasedServerCost(ram)){
			var hostname = ns.purchaseServer("pserv-" + serv, ram);
			await ns.scp("server-hack.js", hostname);
			await ns.scp("grow.script", hostname);
			await ns.scp("weaken.script", hostname);
			await ns.scp("new_hack.script", hostname);
			await ns.exec("server-hack.js", hostname, 1);
			serv++;
		}
	}
}