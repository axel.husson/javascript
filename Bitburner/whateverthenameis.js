var playerServers = ['home'];
var serverChecked = [];
var checkList = [];
/** @param {NS} ns **/
export async function main(ns) {
    await ServersScan(ns, 'home');
    await printArray(ns, serverChecked);
}
/** @param {NS} ns **/
async function ServersScan(ns, target) {
    var servers1 = await ns.scan(target);
    for (var server in servers1) {
        //await ns.tprint(server)
        if (!checkList.includes(servers1[server])) {
            checkList.push(servers1[server]);
        }
    }
    serverChecked.push(target);
    var flag = true;
    while (flag) {
        flag = false;
        for (var i = 0; i < checkList.length; i++) {
            var servers = await ns.scan(checkList[i]);
            if (!serverChecked.includes(checkList)) {
                serverChecked.push(checkList[i]);
            }
            for (var server in servers) {
                if (!checkList.includes(servers[server])) {
                    checkList.push(servers[server]);
                }
            }
        }
    }
    // remove player servers from serverChecked
    for (var server in playerServers) {
        for (var i = 0; i < serverChecked.length; i++) {
            if (serverChecked == playerServers[server]) {
                serverChecked.splice(i, 1);
                i--;
            }
        }
    }
}

async function printArray(ns, serverList) {
	while(true){
		for (var server in serverList) {
			//var niveauPlayer = ns.getHackingLevel();
			//ns.tprint(serverList[server]);
			var droitRoot = ns.hasRootAccess(serverList[server]);
			var argent = ns.getServerMaxMoney(serverList[server]);
			if(droitRoot){
				if(argent != 0){
					var moneyThresh = ns.getServerMaxMoney(serverList[server]) * 0.75;
					var securityThresh = ns.getServerMinSecurityLevel(serverList[server]) + 5;
					ns.print('Droit root OK sur :' + serverList[server])
					if (ns.getServerSecurityLevel(serverList[server]) > securityThresh) {
						ns.exec("weaken.script", 'home', 100, serverList[server]);
						await ns.sleep(2000);
					}
					if (ns.getServerMoneyAvailable(serverList[server]) < moneyThresh){
						ns.exec("grow.script", 'home', 300, serverList[server]);
						await ns.sleep(2000);
					}
					ns.exec("new_hack.script", 'home', 300, serverList[server]);
					await ns.sleep(2000);
					if (ns.getServerSecurityLevel(serverList[server]) > securityThresh) {
						ns.exec("weaken.script", 'home', 300, serverList[server]);
						await ns.sleep(2000);
					}
				}
			}else{
				var niveauPlayer = ns.getHackingLevel();
				var niveauServer = ns.getServerRequiredHackingLevel(serverList[server]);
				
				if(niveauPlayer>=niveauServer){
					//ns.tprint('Serveur Hackable : ' + serverList[server]);
					var nbrPort = ns.getServerNumPortsRequired(serverList[server]);
					//ns.tprint('Nombre de port à hacker : ' + nbrPort);
					var nbrexe = 0;
					if(ns.fileExists("BruteSSH.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("FTPCrack.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("relaySMTP.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("HTTPWorm.exe")){
						nbrexe += 1;
					}
					if(ns.fileExists("SQLInject.exe")){
						nbrexe += 1;
					}
					//ns.tprint('Nombre de fichier executable: ' + nbrexe)
					if(nbrPort <= nbrexe){
						//ns.tprint('Ajout des droits root');
						while(!droitRoot){
							//ns.tprint('Brute SSH');
							if(ns.fileExists("BruteSSH.exe")){
								ns.brutessh(serverList[server]);
								//ns.tprint('Brute FTP');
							}
							if(ns.fileExists("FTPCrack.exe")){
								ns.ftpcrack(serverList[server]);
								//ns.tprint('Brute FTP');
							}
							if(ns.fileExists("relaySMTP.exe")){
								ns.relaysmtp(serverList[server]);
								//ns.tprint('Brute SMTP');
							}
							if(ns.fileExists("HTTPWorm.exe")){
								ns.httpworm(serverList[server]);
								//ns.tprint('Brute SMTP');
							}
							if(ns.fileExists("SQLInject.exe")){
								ns.sqlinject(serverList[server]);
							}
							ns.nuke(serverList[server]);
							//ns.tprint('Nuke');
							var droitRoot = ns.hasRootAccess(serverList[server]);
						}
						//ns.tprint(serverList[server]);
						if(argent != 0){
							await ns.scp("early-hack-template.script", serverList[server]);
							//ns.tprint('depot fichier ok');
							var maxRam = ns.getServerMaxRam(serverList[server]);
							var thread = maxRam / 3;
							//ns.tprint('Nombre de thread utilisé : ' + thread)
							if(thread > 0){
								ns.exec("early-hack-template.script", serverList[server], thread);
							}
						}else{
							var ram = ns.getServerMaxRam(serverList[server]);
							ns.alert('Serveur sans argent trouvé : ' + serverList[server] + ' - Ram dispo : ' + ram);
							await ns.scp("server-hack.js", serverList[server])
							await ns.scp("grow.script", serverList[server])
							await ns.scp("weaken.script", serverList[server])
							await ns.scp("new_hack.script", serverList[server])
							ns.exec("server-hack.js", serverList[server], 1);
						}
					}else{
						//ns.tprint('Impossible de hacker');
					}
				}
	
			}
			//ns.print(serverList[server] + "\n");
			//ns.tprint(serverList[server] + "\n");
		}
	await ns.sleep(2000);
	}
}