/** @param {NS} ns */

export async function main(ns) {
	//initialisation des variables
	var Factionchoice = false
	var Entreprisechoice = false
	var Work = false
	const Augments = await ns.singularity.getOwnedAugmentations()

	//Liste des villes
	const Villes1 = ['Sector-12', 'Volhaven', 'Chongqing', 'Aevum']
	//List entreprise
	const Villes = {'Sector-12' : ['Blade Industries', 'MegaCorp', 'Four Sigma'],
					'Chongqing' : ['KuaiGong International'],
					'Volhaven' : ['NWO', 'OmniTek Incorporated'],
					'Aevum' : ['ECorp', 'Bachman & Associates', 'Clarke Incorporated', 'Fulcrum Technologies']}

	//lance script hacker
	ns.exec("new_hacker_script.js", 'home')

    //lance script sleeves pour gérer leurs actions et achats d'augmentations

	//lance script qui valide les invitations aux factions
	ns.exec("new_faction.js", 'home')
	await ns.sleep(2000)
	//je parcours la liste des villes
	for (const Ville of Villes1){
		//je parcours les entreprise de chaques villes
		const Industries = Villes[Ville]
		for (const industrie of Industries){
			//Check si je fais partie des factions des entreprise
			if(industrie == 'Fulcrum Technologies'){
				var faction = 'Fulcrum Secret Technologies'
			}else{
				var faction = industrie
			}
			var reput = ns.singularity.getFactionRep(faction)
			//Si je ne fais pas partie de la faction
			if (reput == 0 && !EntrepriseChoice){
				//Je stock les infos le nom de l'entreprise et la ville ou elle se trouve
				var entreprise = industrie;
				var cityName = Ville;
				//je sors de la boucle... Peut être...
				ns.alert('Choix de l entreprise : ' + entreprise + 'dans la ville : ' + cityName)
				var EntrepriseChoice = true
				break;
			}
		}
	}
	//Je voyage vers la bonne ville
	if (!EntrepriseChoice){
		var entreprise = 'ECorp';
		var cityName = 'Aevum';
	}
	ns.singularity.travelToCity(cityName)
	ns.alert('Voyage : ' + cityName)

	//lance script new infra achat hacknet
	ns.exec("infrastructure.js", 'home')

	while (true){
		//check ville actuelle
		var cityName = ns.getPlayer().city
		while (ns.hacknet.numHashes() > 4){
			ns.hacknet.spendHashes("Sell for Money");
		}
		//List faction already accept
		const Factions = [];
		const facs = ns.getPlayer().factions
		for(const fac of facs) {
			Factions.push(fac);
		}
		ns.print(Factions)

		//Postuler dans les entreprises
		if (ns.getHackingLevel() >= 250 && !Entreprisechoice){
			const Industries = Villes[cityName]
			for (const industrie of Industries){
				ns.singularity.applyToCompany(industrie, "it")
			}
			var Entreprisechoice = true
		}

		//promotion dans les entreprise
		const entreprises = ns.getPlayer().jobs
		for(let entreprise in entreprises){
			if (ns.getPlayer().skills.charisma > 300){
				var type = 'business'
			}else{
				var type = 'it'
			}
			//ns.tprint("Type : "+type)
			ns.singularity.applyToCompany(entreprise, type)
		}

		//création des programmes
		if(!ns.fileExists("BruteSSH.exe") && !ns.singularity.isBusy()){
			ns.singularity.createProgram('BruteSSH.exe')
		}else if(!ns.fileExists("FTPCrack.exe") && ns.getHackingLevel() > 10 && !ns.singularity.isBusy()){
			ns.singularity.createProgram('FTPCrack.exe')
		}

		//Si on à des factions et qu'on n'a pas fais notre choix
		
		if (Factions.length != 0 && !Factionchoice && ns.fileExists("BruteSSH.exe") && ns.fileExists("FTPCrack.exe")){
			for (const faction of Factions){
				const facaugments = ns.singularity.getAugmentationsFromFaction(faction)
				const facnotpurchasedaugments = facaugments.filter(item => !Augments.includes(item))
				//check si il y a des augmentations a acheter dans la faction
				if (facnotpurchasedaugments.length != 0){
					//Si y en a 
					for (const facnotpurchasedaugment of facnotpurchasedaugments){
						const Pre_Req = ns.singularity.getAugmentationPrereq(facnotpurchasedaugment)
						//et qu'il n'y a pas de pré requis
						if (Pre_Req.length == 0){
							while(!Factionchoice){
								var factionwork = faction;
								var limit_rep_augment = ns.singularity.getAugmentationRepReq(facnotpurchasedaugments[0])
								var prix_augment = ns.singularity.getAugmentationPrice(facnotpurchasedaugments[0])
								var augment = facnotpurchasedaugment
								
								//je fais mon choix
								var Factionchoice = true;
								//ns.sleeve.setToFactionWork(0, factionwork, "Hacking Contracts")
								ns.alert('Vous allez travailler pour la faction ' + factionwork + ' Pour acheter l augmentation ' + facnotpurchasedaugment)
								ns.singularity.workForFaction(factionwork,'hacking')
								
							}
						}
					}
				}				
			}
		}

		if (ns.hacknet.numNodes() > 10){
			ns.exec("new_achat_server.js", "home")
		}
		if (Factionchoice && Entreprisechoice){
			if (limit_rep_augment < ns.singularity.getFactionRep(factionwork) && !Work){
				ns.kill('infrastructure.js', 'home')
				ns.singularity.workForCompany(entreprise);
				var Work = true
			}
		}

		//achat de l'augmentation
		var argent = ns.getServerMoneyAvailable('home')
		if (Factionchoice){
			if (argent > prix_augment && limit_rep_augment < ns.singularity.getFactionRep(factionwork)){
				ns.singularity.purchaseAugmentation(factionwork, augment)
				var Factionchoice = false
				var Work = false
				Augments.push(augment)
			}
		}

		//Je suis en fin de boucle et je n'ai rien a faire
		if (!ns.singularity.isBusy() && ns.fileExists("BruteSSH.exe") && ns.fileExists("FTPCrack.exe")){
			if (Factions.length != 0){
				ns.singularity.workForFaction(Factions[0],'hacking')
			}else{
				ns.singularity.universityCourse('Rothman University', 'Study Computer Science')
			}
		}
		
		await ns.sleep(1000)
	}
}