/** @param {NS} ns */
export async function main(ns) {
	//Liste des villes
	const Villes1 = ['Sector-12', 'Chongqing', 'Volhaven', 'Aevum']
	//liste des entreprise par villes
	const Villes = {'Sector-12' : ['MegaCorp', 'Four Sigma', 'Blade Industries'],
					'Chongqing' : ['KuaiGong International'],
					'Volhaven' : ['NWO', 'OmniTek Incorporated'],
					'Aevum' : ['ECorp', 'Bachman & Associates', 'Clarke Incorporated', 'Fulcrum Technologies']}
	//je parcours la liste des villes
	for (const Ville of Villes1){
		//je parcours les entreprise de chaques villes
		const Industries = Villes[Ville]
		for (const industrie of Industries){
			//Check si je fais partie des factions des entreprise
			if(industrie == 'Fulcrum Technologies'){
				var faction = 'Fulcrum Secret Technologies'
			}else{
				var faction = industrie
			}
			var reput = ns.singularity.getFactionRep(faction)
			//Si je ne fais pas partie de la faction
			if (reput == 0){
				//Je stock les infos le nom de l'entreprise et la ville ou elle se trouve
				var entreprise = industrie;
				var cityName = Ville;
				//je sors de la boucle... Peut être...
				ns.alert('Choix de l entreprise : ' + entreprise + 'dans la ville : ' + cityName)
				var EntrepriseChoice = true
				break;
			}
		}
	}
	//Je voyage vers la bonne ville
	if (!EntrepriseChoice){
		var entreprise = 'MegaCorp';
		var cityName = 'Sector-12';
	}
	ns.singularity.travelToCity(cityName)
	var limit_rep = 4000
	while (true){
		const works = ns.singularity.getCurrentWork()
			if (ns.singularity.isBusy()){
				if (works.cyclesWorked){
					if (works.cyclesWorked > limit_rep){
						ns.singularity.stopAction()
					}
				}
			}else{
				if (ns.getPlayer().skills.charisma > 300){
						var type = 'business'
				}else{
					var type = 'it'
				}
				ns.singularity.applyToCompany(entreprise, type);
				ns.singularity.workForCompany(entreprise);
			}
		await ns.sleep(1000)
	}
}